import Vue from "vue";
import VueRouter from "vue-router";
import BootstrapVue from "bootstrap-vue";
import App from "./App.vue";
import { routes } from "./routes";
import "bootstrap/dist/css/bootstrap.css";
import vmodal from "vue-js-modal";
import store from "./store";

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(vmodal);

const router = new VueRouter({
  routes,
  store,
  mode: "history",
});

new Vue({
  el: "#app",
  router,
  store,
  render: (h) => h(App),
});
