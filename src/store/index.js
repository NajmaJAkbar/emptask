import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    title: "Task Management",
    title1: "Employee Management",
    newTodo: "",
    taskValue: "Choose The Task Type",
    newDesc: "",
    todos: [],
    newEmployee: "",
    newAge: "",
    employees: [],
  },

  mutations: {
    setNewTodo(state, value) {
      state.newTodo = value;
    },
    setNewEmployee(state, value) {
      state.newEmployee = value;
    },
    setNewAge(state, value) {
      state.newAge = value;
    },
    setTaskValue(state, value) {
      state.taskValue = value;
    },
    setNewDesc(state, value) {
      state.newDesc = value;
    },
    addTodo(state) {
      state.todos.push({
        title: state.newTodo,
        taskVal: state.taskValue,
        description: state.newDesc,
        done: false,
      });
    },
    DELETE_TODO(state) {
      var todos = state.todos;
      todos.splice(todos.indexOf(todos), 1);
      state.todos = todos;
      state.newTodo = todos.body;
    },
    addEmployee(state) {
      state.employees.push({
        name: state.newEmployee,
        age: state.newAge,
        taskVal: state.taskValue,
        done: false,
      });
    },
  },

  actions: {
    addTodo(context) {
      context.commit("addTodo");
      context.commit("setNewTodo", "");
      context.commit("setTaskValue", "");
      context.commit("setNewDesc", "");
    },
    addEmployee(context) {
      context.commit("addEmployee");
      context.commit("setNewEmployee", "");
      context.commit("setTaskValue", "");
      context.commit("setNewAge", "");
    },
    deleteTodo({ commit }, todo) {
      commit("DELETE_TODO", todo);
    },
  },
  getters: {
    getdata: (state) => {
      return state.todos;
    },
    employeesdata: (state) => {
      return state.employees;
    },
  },

  modules: {},
});
