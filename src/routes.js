const Home = (resolve) => {
  require.ensure(["./components/Home.vue"], () => {
    resolve(require("./components/Home.vue"));
  });
};

const Employee = (resolve) => {
  require.ensure(["./components/employee/Employee.vue"], () => {
    resolve(require("./components/employee/Employee.vue"));
  });
};
const Task = (resolve) => {
  require.ensure(["./components/tasks/Task.vue"], () => {
    resolve(require("./components/tasks/Task.vue"));
  });
};

export const routes = [
  {
    path: "",
    name: "Home",
    components: {
      default: Home,
    },
  },
  {
    path: "/employee",
    name: "Employee",
    components: {
      default: Employee,
    },
  },
  {
    path: "/Task",
    name: "Task",
    components: {
      default: Task,
    },
  },
  // {path:'/redirect-me', redirect:{name:'Employee'}},
  { path: "*", redirect: "/" },
];
